package geom

import "math"

type Vec3 struct {
	X, Y, Z float32
}

func NewVec3(x, y, z float32) Vec3 {
	return Vec3{
		X: x,
		Y: y,
		Z: z,
	}
}

func VecZero() Vec3 {
	return Vec3{
		X: 0, Y: 0, Z: 0,
	}
}
func VecOne() *Vec3 {
	return &Vec3{
		X: 1, Y: 1, Z: 1,
	}
}

func VecX(a float32) *Vec3 {
	return &Vec3{
		X: a, Y: 0, Z: 0,
	}
}
func VecY(a float32) *Vec3 {
	return &Vec3{
		X: 0, Y: a, Z: 0,
	}
}
func VecZ(a float32) *Vec3 {
	return &Vec3{
		X: 0, Y: 0, Z: a,
	}
}

func (fst *Vec3) Cross(snd *Vec3) *Vec3 {
	return &Vec3{
		X: fst.Y*snd.Z - fst.Z*snd.Y,
		Y: fst.Z*snd.X - fst.X*snd.Z,
		Z: fst.X*snd.Y - fst.Y*snd.X,
	}
}

func (fst *Vec3) Norm() float32 {
	return float32(math.Sqrt(float64(fst.X*fst.X + fst.Y*fst.Y + fst.Z*fst.Z)))
}

func (fst *Vec3) Normalize() *Vec3 {
	return fst.NormalizeL(1)
}

func (fst *Vec3) NormalizeL(l float32) *Vec3 {
	return fst.MulNum(l / fst.Norm())
}

func (fst *Vec3) Neg() *Vec3 {
	return fst.MulNum(-1)
}

func (fst *Vec3) MulVec(snd *Vec3) float32 {
	return fst.X*snd.X + fst.Y*snd.Y + fst.Z*snd.Z
}

func (fst *Vec3) MulNum(c float32) *Vec3 {
	return &Vec3{
		X: fst.X * c,
		Y: fst.Y * c,
		Z: fst.Z * c,
	}
}

func (fst *Vec3) AddVec(snd *Vec3) *Vec3 {
	return &Vec3{
		X: fst.X + snd.X,
		Y: fst.Y + snd.Y,
		Z: fst.Z + snd.Z,
	}
}

func (fst *Vec3) SubVec(snd *Vec3) *Vec3 {
	return &Vec3{
		X: fst.X - snd.X,
		Y: fst.Y - snd.Y,
		Z: fst.Z - snd.Z,
	}
}
