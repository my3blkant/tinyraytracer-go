package renderer

import (
	"math"

	"bitbucket.org/my3blkant/tinyraytracer-go/geom"
)

type Sphere struct {
	Center   geom.Vec3
	Radius   float32
	Material Material
}

func RaySphereIntersect(orig *geom.Vec3, dir *geom.Vec3, s *Sphere, t0 *float32) bool {
	var (
		l   = s.Center.SubVec(orig)
		tca = l.MulVec(dir)
		d2  = l.MulVec(l) - tca*tca
	)
	if d2 > s.Radius*s.Radius {
		return false
	}
	var (
		thc = float32(math.Sqrt(float64(s.Radius*s.Radius - d2)))
		t1  = tca + thc
	)
	*t0 = tca - thc
	if *t0 < 1e-3 {
		*t0 = t1
	}
	if *t0 < 1e-3 {
		return false
	}

	return true
}
