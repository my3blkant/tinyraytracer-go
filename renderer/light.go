package renderer

import "bitbucket.org/my3blkant/tinyraytracer-go/geom"

type Light struct {
	Position  geom.Vec3
	Intensity float32
}
