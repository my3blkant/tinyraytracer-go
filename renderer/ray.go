package renderer

import (
	"bufio"
	"fmt"
	"math"
	"os"

	"bitbucket.org/my3blkant/tinyraytracer-go/geom"
)

func Reflect(i *geom.Vec3, n *geom.Vec3) *geom.Vec3 {
	return i.SubVec(n.MulNum(2).MulNum(i.MulVec(n)))
}

func Refract(i *geom.Vec3, n *geom.Vec3, eta_t float32) *geom.Vec3 {
	return RefractI(i, n, eta_t, 1)
}

func RefractI(i *geom.Vec3, n *geom.Vec3, eta_t float32, eta_i float32) *geom.Vec3 {
	var cosi = float32(-math.Max(-1, math.Min(1, float64(i.MulVec(n)))))
	if cosi < 0 {
		return RefractI(i, n.Neg(), eta_i, eta_t)
	}
	var (
		eta = eta_i / eta_t
		k   = 1 - eta*eta*(1-cosi*cosi)
	)
	if k < 0 {
		zero := geom.VecX(1)
		return zero
	} else {
		return i.MulNum(eta).AddVec(n.MulNum(eta*cosi - float32(math.Sqrt(float64(k)))))
	}
}

func abs32(x float32) float32 {
	if x < 0 {
		return -x
	}
	return x
}

func min32(x, y float32) float32 {
	if x < y {
		return x
	}
	return y
}

func max32(x, y float32) float32 {
	if x < y {
		return y
	}
	return x
}

func SceneIntersect(orig *geom.Vec3, dir *geom.Vec3, spheres []Sphere, hit *geom.Vec3, n *geom.Vec3, material *Material) bool {
	var spheres_dist float32 = math.MaxFloat32
	for _, s := range spheres {
		var dist_i float32
		if RaySphereIntersect(orig, dir, &s, &dist_i) && dist_i < spheres_dist {
			spheres_dist = dist_i
			*hit = *orig.AddVec(dir.MulNum(dist_i))
			*n = *(hit.SubVec(&s.Center)).Normalize()
			*material = s.Material
		}
	}
	var checkerboard_dist float32 = math.MaxFloat32
	if abs32(dir.Y) > 1e-3 {
		var (
			d  = -(orig.Y + 4) / dir.Y
			pt = orig.AddVec(dir.MulNum(d))
		)

		if d > 1e-3 && abs32(pt.X) < 10 && pt.Z < -10 && pt.Z > -30 && d < spheres_dist {
			checkerboard_dist = d
			*hit = *pt
			*n = *geom.VecY(1)
			if (int32(hit.X*.5+100)+int32(hit.Z*0.5))&1 == 1 {
				material.Diffuse_color = geom.NewVec3(0.3, 0.3, 0.3)
			} else {
				material.Diffuse_color = geom.NewVec3(0.3, 0.2, 0.1)
			}
		}

	}
	return min32(spheres_dist, checkerboard_dist) < 1000
}

func CastRay(orig *geom.Vec3, dir *geom.Vec3, spheres []Sphere, lights []Light) geom.Vec3 {
	return CastRayDepth(orig, dir, spheres, lights, 0)

}

func CastRayDepth(orig *geom.Vec3, dir *geom.Vec3, spheres []Sphere, lights []Light, depth int32) geom.Vec3 {
	var (
		point, n geom.Vec3
	)
	material := NewMaterial()

	if depth > 4 || !SceneIntersect(orig, dir, spheres, &point, &n, &material) {
		return geom.NewVec3(0.2, 0.7, 0.8)
	}

	var (
		reflect_dir              = Reflect(dir, &n).Normalize()
		refract_dir              = Refract(dir, &n, material.Refractive_index).Normalize()
		reflect_color            = CastRayDepth(&point, reflect_dir, spheres, lights, depth+1)
		refract_color            = CastRayDepth(&point, refract_dir, spheres, lights, depth+1)
		diffuse_light_intensity  float32
		specular_light_intensity float32
	)

	for _, light := range lights {
		var (
			light_dir           = light.Position.SubVec(&point).Normalize()
			shadow_pt, trashnrm geom.Vec3
			trashmat            Material
		)
		if SceneIntersect(&point, light_dir, spheres, &shadow_pt, &trashnrm, &trashmat) &&
			shadow_pt.SubVec(&point).Norm() < light.Position.SubVec(&point).Norm() {
			continue
		}
		diffuse_light_intensity += light.Intensity * float32(max32(0, light_dir.MulVec(&n)))
		specular_light_intensity += float32(math.Pow(float64(max32(0,
			-(Reflect(light_dir.Neg(), &n).MulVec(dir)))),
			float64(material.Specular_exponent))) * light.Intensity
	}

	return *material.Diffuse_color.MulNum(diffuse_light_intensity * material.Albedo[0]).AddVec(
		geom.VecOne().MulNum(specular_light_intensity * material.Albedo[1])).AddVec(
		reflect_color.MulNum(material.Albedo[2])).AddVec(
		refract_color.MulNum(material.Albedo[3]))

}

func maxF(a, b float32) float32 {
	if a < b {
		return b
	}
	return a
}
func Render(spheres []Sphere, lights []Light) {
	width := 1024
	height := 768
	fov := float32(math.Pi / 3.)
	framebuffer := make([]geom.Vec3, width*height)

	for j := 0; j < height; j++ {
		for i := 0; i < width; i++ {
			dir_x := float32(i) + .5 - float32(width)/2.
			dir_y := -(float32(j) + .5) + float32(height)/2.
			dir_z := -float32(height) / (2. * float32(math.Tan(float64(fov)/2.)))
			zero := geom.VecZero()
			dir := geom.NewVec3(dir_x, dir_y, dir_z)
			color := CastRay(&zero, dir.Normalize(), spheres, lights)
			framebuffer[i+j*width] = color
		}
	}

	f, _ := os.Create("./out.ppm")
	buf := bufio.NewWriter(f)
	buf.WriteString(fmt.Sprintf("P6\n%d %d\n255\n", width, height))
	for _, c := range framebuffer {
		max := maxF(c.X, maxF(c.Y, c.Z))
		if max > 1 {
			c = *c.MulNum(1 / max)
		}
		buf.Write([]byte{byte(255 * c.X), byte(255 * c.Y), byte(255 * c.Z)})
	}
	buf.Flush()
	f.Close()
}

func TestRender(spheres []Sphere, lights []Light) {
	width := 1024
	height := 768
	i := 110
	j := 590
	fov := float32(math.Pi / 3.)
	dir_x := float32(i) + .5 - float32(width)/2.
	dir_y := -(float32(j) + .5) + float32(height)/2.
	dir_z := -float32(height) / (2. * float32(math.Tan(float64(fov)/2.)))

	zero := geom.VecZero()
	dir := geom.NewVec3(dir_x, dir_y, dir_z)
	color := CastRay(&zero, dir.Normalize(), spheres, lights)
	println("%f %f %f", color.X, color.Y, color.Z)

}
