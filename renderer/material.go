package renderer

import "bitbucket.org/my3blkant/tinyraytracer-go/geom"

type Material struct {
	Refractive_index  float32
	Albedo            [4]float32
	Diffuse_color     geom.Vec3
	Specular_exponent float32
}

func NewMaterial() Material {
	return Material{
		Refractive_index:  1,
		Albedo:            [4]float32{1, 0, 0, 0},
		Diffuse_color:     geom.VecZero(),
		Specular_exponent: 0,
	}
}

type Color [3]float32
