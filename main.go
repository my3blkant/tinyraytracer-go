package main

import (
	"bitbucket.org/my3blkant/tinyraytracer-go/geom"
	"bitbucket.org/my3blkant/tinyraytracer-go/renderer"
)

func main() {
	ivory := renderer.Material{
		Refractive_index:  1.0,
		Albedo:            [4]float32{0.6, 0.3, 0.1, 0.0},
		Diffuse_color:     geom.NewVec3(0.4, 0.4, 0.3),
		Specular_exponent: 50.,
	}
	glass := renderer.Material{
		Refractive_index:  1.5,
		Albedo:            [4]float32{0.0, 0.5, 0.1, 0.8},
		Diffuse_color:     geom.NewVec3(0.6, 0.7, 0.8),
		Specular_exponent: 125.,
	}
	red_rubber := renderer.Material{
		Refractive_index:  1.0,
		Albedo:            [4]float32{0.9, 0.1, 0.0, 0.0},
		Diffuse_color:     geom.NewVec3(0.3, 0.1, 0.1),
		Specular_exponent: 10.,
	}
	mirror := renderer.Material{
		Refractive_index:  1.0,
		Albedo:            [4]float32{0.0, 10.0, 0.8, 0.0},
		Diffuse_color:     geom.NewVec3(1., 1., 1.),
		Specular_exponent: 1425.,
	}
	spheres := []renderer.Sphere{
		{
			Center:   geom.NewVec3(-3, 0, -16),
			Radius:   2,
			Material: ivory,
		},
		{
			Center:   geom.NewVec3(-1, -1.5, -12),
			Radius:   2,
			Material: glass,
		},
		{
			Center:   geom.NewVec3(1.5, -0.5, -18),
			Radius:   3,
			Material: red_rubber,
		},
		{
			Center:   geom.NewVec3(7, 5, -18),
			Radius:   4,
			Material: mirror,
		},
	}

	lights := []renderer.Light{
		{
			Position:  geom.NewVec3(-20, 20, 20),
			Intensity: 1.5,
		},
		{
			Position:  geom.NewVec3(30, 50, -25),
			Intensity: 1.8,
		},
		{
			Position:  geom.NewVec3(30, 20, 30),
			Intensity: 1.7,
		},
	}
	renderer.Render(spheres, lights)
}
