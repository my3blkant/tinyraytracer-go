# Golang port of C++ [ssloy/tinyraytracer](https://github.com/ssloy/tinyraytracer)

Demo project for self-study only. Porting of C++ code to Golang.

Simple ray tracer with spheres and floor-like surface.

Result image:

![](https://bitbucket.org/my3blkant/tinyraytracer-go/raw/88fafec4dd60c2fa9f710941566dae07d149c38c/out.jpg)

## Run:
```sh
go get bitbucket.org/my3blkant/tinyraytracer-go
tinyraytracer-go
```
Then open out.ppm in current directory.